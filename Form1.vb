﻿Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim value1 = -3
        Dim value2 = 1
        Dim w = 5
        Dim h = 5

        Dim X = Math.Ceiling(w / 2) + 1 + value1
        Dim Y = Math.Ceiling(w / 2) + value2
        Console.WriteLine(ulam(w, h, X, Y))
    End Sub

    Function validWidthAndHeight_(ByVal w, ByVal h) As Boolean
        Try
            Return w = h Or w = h + 1
        Catch ex As Exception
            Console.WriteLine("Invalid W and H combination")
            Return False
        End Try

    End Function

    Function validXandYCoords_(ByVal w, ByVal h, ByVal px, ByVal py) As Boolean
        Return px > 0 AndAlso px <= w AndAlso py > 0 AndAlso py <= h
    End Function

    Function determineStartCoords_(ByVal w, ByVal h) As String
        Dim f = Math.Floor(w / 2) + (w Mod 2) & " " & Math.Floor(h / 2) + 1
        Return f
    End Function

    Function ulam(ByVal w, ByVal h, ByVal px, ByVal py) As Integer

        If Not validWidthAndHeight_(w, h) Then
            Console.WriteLine("Invalid W and H combination")
            Return ""
        End If


        If Not validXandYCoords_(w, h, px, py) Then
            Console.WriteLine("Invalid PX and PY combination")
            Return ""
        End If

        Dim startCoords() As String = Split(determineStartCoords_(w, h), " ")
        Dim x As Double = CDbl(startCoords(0))
        Dim y As Double = CDbl(startCoords(1))

        Dim value As String = 1 ' the value that increments (1, 2, 3 etc)
        Dim count As String = 1 ' represents the <count> in the comment above

        ' Starting directions, and previous directions
        Dim xdir As String = +1, ydir = 0
        Dim xdirprev As String = +1, ydirprev = +1

        While (value < 9999999)

            ' Do twice
            For i As Integer = 0 To 1

                ' Do <count> times
                For c As Integer = 0 To count - 1

                    ' Are we at the desired coords?
                    If x = px AndAlso y = py Then
                        Return value
                    End If


                    ' Increment value
                    value += 1

                    ' Move
                    x += xdir
                    y += ydir

                Next

                ' Adjust direction
                If (xdir = 0) Then
                    xdir = xdirprev * -1
                    ydirprev = ydir
                    ydir = 0
                Else
                    ydir = ydirprev * -1
                    xdirprev = xdir
                    xdir = 0
                End If
            Next

            count += 1


        End While

    End Function
End Class
